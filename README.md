CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Browser Storage Commands module provides AJAX commands to add and delete
data from browser storage (either local or session).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/browser_storage_commands

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/browser_storage_commands


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Barrett Langton (bdlangton) - https://www.drupal.org/u/bdlangton
