(function (Drupal) {

  Drupal.AjaxCommands.prototype.storageAdd = function (ajax, response, status) {
    // Get the correct browser storage.
    let storage = localStorage;
    if (response.storage === 'session') {
      storage = sessionStorage;
    }

    // Get the browser storage object.
    let data = storage.getItem(response.key) || {};
    if (data != null && Object.keys(data).length > 0) {
      data = JSON.parse(data);
    }

    if (data == null) {
      data = {};
    }

    // Add all provided values to the object.
    if (response.data != null) {
      for (var entry in response.data) {
        data[entry] = response.data[entry];
      }
    }

    // Save the new object.
    storage.setItem(response.key, JSON.stringify(data));
  }

  Drupal.AjaxCommands.prototype.storageDelete = function (ajax, response, status) {
    // Get the correct browser storage.
    let storage = localStorage;
    if (response.storage === 'session') {
      storage = sessionStorage;
    }

    // Get the browser storage object. If it's empty, there is nothing to do.
    let data = storage.getItem(response.key) || {};
    if (data != null && Object.keys(data).length == 0) {
      return;
    }

    if (data == null) {
      data = {};
    }

    data = JSON.parse(data);

    // If there are data elements to delete, then delete those individually and
    // resave the object. If no data elements are provided, then delete the
    // entire object from browser storage.
    if (response.data != null && Object.keys(response.data).length > 0) {
      for (var entry in response.data) {
        delete data[entry];
      }
      storage.setItem(response.key, JSON.stringify(data));
    }
    else {
      storage.removeItem(response.key);
    }
  }

})(Drupal);
