<?php

namespace Drupal\browser_storage_commands\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Generic AJAX command for adding info to browser storage.
 *
 * This command instructs the client to add data provided to the browser storage
 * key also provided.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.storageAdd()
 * defined in js/ajax.js.
 *
 * @ingroup ajax
 */
class StorageAddCommand extends StorageBase implements CommandInterface {

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'storageAdd',
      'key' => $this->key,
      'data' => $this->data,
      'storage' => $this->storage,
    ];
  }

}
