<?php

namespace Drupal\browser_storage_commands\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Generic AJAX command for deleting info to browser storage.
 *
 * This command instructs the client to delete data keys provided from the
 * browser storage key also provided. If no data keys are provided, it deletes
 * the entire storage for the key.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.storageDelete()
 * defined in js/ajax.js.
 *
 * @ingroup ajax
 */
class StorageDeleteCommand extends StorageBase implements CommandInterface {

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'storageDelete',
      'key' => $this->key,
      'data' => $this->data,
      'storage' => $this->storage,
    ];
  }

}
