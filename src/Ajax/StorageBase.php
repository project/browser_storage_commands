<?php

namespace Drupal\browser_storage_commands\Ajax;

/**
 * Base class for storage commands.
 *
 * @ingroup ajax
 */
class StorageBase {

  /**
   * The key for storing the data in browser storage.
   *
   * @var string
   */
  protected $key;

  /**
   * Data to add or remove from browser storage.
   *
   * @var array
   */
  protected $data;

  /**
   * Which storage to use (local or session).
   *
   * @var string
   */
  protected $storage;

  /**
   * Constructs a storage command object.
   *
   * @param string $key
   *   The key for storing data in browser storage.
   * @param array $data
   *   Data to add to local storage.
   * @param string $storage
   *   Which storage to use.
   */
  public function __construct($key = 'Drupal.localstorage', array $data = [], $storage = 'local') {
    $this->key = $key;
    $this->data = $data;
    $this->storage = $storage;
  }

}
